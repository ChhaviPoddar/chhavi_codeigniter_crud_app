<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$this->homep();
	}
	
	public function homep()
	{
		echo "controller  ";
		$this->load->model('m_home');

		//if(isset($_get['number']))
		$data['newuser']=$this->m_home->put_in_db();
		$data['title']='Chhavi\'s CodeIgniter';
		$data['users']=$this->m_home->getUsers();
		$this->load->view('home',$data);
	}

	public function add()
	{
		$this->load->model('m_home');
		
		$this->load->view('add_form');
	}

	
}
